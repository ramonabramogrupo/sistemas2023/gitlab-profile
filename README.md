![ramon](imgs/ramon.jpeg)

# ![logo](imgs/rpeque.png)Hola 🖐 mi nombre es Ramon Abramo 

Soy profesor desde hace mas de 20 años.

Este grupo le he creado para el certificado de Sistemas de informacion.

Este grupo le he credo para que los alumnos sigan los ejemplos realizados en clase y para las practicas.


<details>
<summary> 

# Estructura completa del repositorio (clic para mostrar)

</summary>

- [X] Pagina Inicio

- [ ] SQL
  - [ ] Ejemplos
  - [ ] Practicas

- [ ] CMS
  - [ ] Ejemplos
  - [ ] Practicas

  </details>


![portada](imgs/1.jpg)

# Certificado de Profesionalidad en Sistemas de Gestión de la Información (IFCD0211)

El Certificado de Profesionalidad en Sistemas de Gestión de la Información (IFCD0211) es una acreditación que te prepara para convertirte en un experto en la implementación, manejo, clasificación y consulta de información almacenada en bases de datos.

Aquí están los detalles clave:

1. Duración total: El certificado consta de 510 horas de formación.
2. Módulo de formación práctica en centros de trabajo: Se dedican 80 horas a la práctica en empresas o entornos laborales reales.
3. Nivel de cualificación: Este certificado se encuentra en el nivel 3, lo que implica un nivel avanzado de competencias profesionales.
4. Contenido del certificado: 

Un certificado de profesionalidad de sistemas de información es un documento oficial que acredita la competencia profesional de una persona para realizar actividades relacionadas con la gestión de la información en una organización

![sql](imgs/sql2.jpg){width=100%}

Este certificado te permite trabajar en diferentes sectores y puestos, como administrador de bases de datos, programador de sistemas, analista de datos, consultor de sistemas de información, etc. Además, te facilita el acceso a otros certificados de profesionalidad o a estudios superiores relacionados con la informática y las telecomunicaciones

